package br.com.mauda.seminario.cientificos.model;

public class Curso {

    private Long id;
    private String nome;

    private AreaCientifica areaCientifica;

    // Construtor
    public Curso(AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        this.areaCientifica.adicionarCurso(this);
    }

    // GETTERS and SETTERS
    public Long getId() {
        return this.id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }

}
