package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario {

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();

    // Construtor
    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.adicionarAreaCientifica(areaCientifica);
        this.adicionarProfessor(professor);
        this.qtdInscricoes = qtdInscricoes;

        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
    }

    // Metodos UML
    public void adicionarAreaCientifica(AreaCientifica areaCientifica) {
        if (Boolean.FALSE.equals(this.possuiAreaCientifica(areaCientifica))) {
            this.areasCientificas.add(areaCientifica);
        }
    }

    public void adicionarInscricao(Inscricao inscricao) {
        if (Boolean.FALSE.equals(this.possuiInscricao(inscricao))) {
            this.inscricoes.add(inscricao);
        }
    }

    public void adicionarProfessor(Professor professor) {
        if (Boolean.FALSE.equals(this.possuiProfessor(professor))) {
            this.professores.add(professor);
            professor.adicionarSeminario(this);
        }
    }

    public Boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areasCientificas.contains(area);
    }

    public Boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public Boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    // GETTERS and SETTERS
    public Long getId() {
        return this.id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }
}
