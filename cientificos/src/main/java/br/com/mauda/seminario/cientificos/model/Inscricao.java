package br.com.mauda.seminario.cientificos.model;

import java.util.Date;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;

    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    // Construtor
    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.adicionarInscricao(this);
    }

    // Metodos UML
    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (this.situacao.equals(SituacaoInscricaoEnum.DISPONIVEL) && new Date().before(this.seminario.getData())) {
            this.estudante = estudante;
            estudante.adicionarInscricao(this);
            this.direitoMaterial = direitoMaterial;
            this.situacao = SituacaoInscricaoEnum.COMPRADO;
        }
    }

    public void cancelarCompra() {
        if (this.situacao.equals(SituacaoInscricaoEnum.COMPRADO)) {
            this.estudante.removerInscricao(this);
            this.estudante = null;
            this.direitoMaterial = null;
            this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        }
    }

    public void realizarCheckIn() {
        if (this.situacao.equals(SituacaoInscricaoEnum.COMPRADO) && new Date().before(this.seminario.getData())) {
            this.situacao = SituacaoInscricaoEnum.CHECKIN;
        }
    }

    // GETTERS and SETTERS
    public Long getId() {
        return this.id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }
}
