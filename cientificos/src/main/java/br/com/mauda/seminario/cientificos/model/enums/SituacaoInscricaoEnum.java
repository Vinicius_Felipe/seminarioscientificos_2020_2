package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "Disponível"),
    COMPRADO(2L, "Comprado"),
    CHECKIN(3L, "Check-in");

    private Long id;
    private String nome;

    // Construtor
    SituacaoInscricaoEnum(Long id, String nome) {
        this.nome = nome;
        this.id = id;
    }

    // GETTERS and SETTERS
    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}
